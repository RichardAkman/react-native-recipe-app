import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, TouchableNativeFeedback, Platform} from 'react-native';

const CategoryItem = props => {
    let TouchableComponent = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableComponent = TouchableNativeFeedback;
    }

    return (
        <View style={styles.itemContainer}>
            <TouchableComponent style={styles.touchable} onPress={props.onSelect}>
                <View style={{...styles.wrapper, ...{backgroundColor: props.color}}}>
                    <Text style={styles.title} numberOfLines={2}>{props.title}</Text>
                </View>
            </TouchableComponent>
        </View>
    )
};

const styles = StyleSheet.create({
    itemContainer: {
        flex: 1,
        margin: 15,
        height: 100,
        borderRadius: 10,
        overflow: 'hidden'
    },
    touchable: {
        flex: 1
    },
    wrapper: {
        flex: 1,
        borderRadius: 10,
        shadowOpacity: 0.2,
        shadowRadius: 15,
        elevation: 3,
        padding: 7,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 18,
        textAlign: 'right'
    }
});

export default CategoryItem;
