import React from 'react';
import {
    StyleSheet,
    Text,
} from 'react-native';

const DefaultText = props => {
    return (
        <Text style={{...styles.wrapper, ...props.style}}>{props.children}</Text>
    )
};

const styles = StyleSheet.create({
    wrapper: {
        fontFamily: 'open-sans'
    },
});

export default DefaultText;
