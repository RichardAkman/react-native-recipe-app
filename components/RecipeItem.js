import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    TouchableNativeFeedback,
    Platform,
    ImageBackground
} from 'react-native';
import DefaultText from "./DefaultText";

const RecipeItem = props => {
    let TouchableComponent = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableComponent = TouchableNativeFeedback;
    }

    return (
        <View style={styles.itemContainer}>
            <TouchableComponent style={styles.touchable} onPress={props.onSelect}>
                <View style={styles.recipeContainer}>
                    <View style={{...styles.recipeRow, ...styles.recipeHeader}}>
                        <ImageBackground
                            style={styles.image}
                            source={{
                                uri: props.imageUrl
                            }}
                            fadeDuration={500}
                        >
                            <View style={styles.titleWrapper}>
                                <DefaultText style={styles.title}>{props.title}</DefaultText>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{...styles.recipeRow, ...styles.recipeBody}}>
                        <DefaultText>{props.duration}</DefaultText>
                        <DefaultText>{props.complexity}</DefaultText>
                        <DefaultText>{props.affordability}</DefaultText>
                    </View>
                </View>
            </TouchableComponent>
        </View>
    )
};

const styles = StyleSheet.create({
    itemContainer: {
        flex: 1,
        margin: 15,
        height: 200,
        borderRadius: 10,
        elevation: 3,
        overflow: 'hidden'
    },
    touchable: {
        flex: 1
    },
    recipeContainer: {},
    recipeRow: {
        flexDirection: 'row'
    },
    recipeHeader: {
        height: '80%'
    },
    recipeBody: {
        height: '20%',
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    titleWrapper: {
        backgroundColor: 'rgba(0,0,0,0.4)',
        width: '100%'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        textAlign: 'right',
        color: 'white',
        padding: 5
    },
    image: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
});

export default RecipeItem;
