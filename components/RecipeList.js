import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import RecipeItem from "./RecipeItem";
import {useSelector} from "react-redux";

const RecipeList = props => {
    const favoriteRecipes = useSelector(state => {
        return state.recipes.favoriteRecipes
    })

    const renderItem = (itemData) => {
        const isFavoriteRecipe = favoriteRecipes.some(recipe => recipe.id === itemData.item.id)

        return <RecipeItem
            title={itemData.item.title}
            duration={itemData.item.duration}
            complexity={itemData.item.complexity}
            affordability={itemData.item.affordability}
            imageUrl={itemData.item.imageUrl}
            onSelect={() => {
                props.navigation.navigate({
                    routeName: 'MealDetail',
                    params: {
                        recipeID: itemData.item.id,
                        recipeTitle: itemData.item.title,
                        isFavorite: isFavoriteRecipe
                    }
                })
            }}
        />
    }

    return (
        <FlatList style={styles.listStyle} numColumns={1} data={props.recipes} renderItem={renderItem}/>
    )
}

const styles = StyleSheet.create({
    listStyle: {
        width: '100%'
    }
});

export default RecipeList
