import React from "react";
import {Platform, Text} from "react-native";
import {createStackNavigator} from 'react-navigation-stack'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {createAppContainer} from 'react-navigation'
import CategoriesScreen from "../screens/CategoriesScreen";
import CategoryMealsScreen from "../screens/CategoryMealsScreen";
import MealDetailsScreen from "../screens/MealDetailsScreen";
import FavoritesScreen from "../screens/FavoritesScreen";
import FiltersScreen from "../screens/FiltersScreen";
import Colors from "../constants/colors";
import {Entypo, MaterialIcons} from '@expo/vector-icons'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'
import {createDrawerNavigator} from 'react-navigation-drawer'

const defaultNavigationOptions = {
    headerStyle: {
        backgroundColor: Colors.primary
    },
    headerTitleStyle: {
        fontFamily: 'open-sans-bold',
    },
    headerBackTitleStyle: {
        fontFamily: 'open-sans',
    },
    headerTintColor: 'white'
}

const HomeNavigationConfig = createStackNavigator({
    Categories: {
        screen: CategoriesScreen,
        navigationOptions: {
            title: 'Categories'
        }
    },
    CategoryMeals: {
        screen: CategoryMealsScreen,
    },
    MealDetail: {
        screen: MealDetailsScreen
    }
}, {
    defaultNavigationOptions
});

const FavoriteNavigationConfig = createStackNavigator({
    Favorites: {
        screen: FavoritesScreen,
        navigationOptions: {
            title: 'Favorites'
        }
    },
    MealDetail: {
        screen: MealDetailsScreen
    }
}, {
    defaultNavigationOptions
});

const FilterNavigationConfig = createStackNavigator({
    Favorites: {
        screen: FiltersScreen,
        navigationOptions: {
            title: 'Filters'
        }
    },
}, {
    defaultNavigationOptions
});

const tabConfig = {
    Home: {
        screen: HomeNavigationConfig,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Entypo name='cake' size={25} color={tabInfo.tintColor}/>
            },
            tabBarLabel: <Text style={{fontFamily: 'open-sans-bold'}}>Recipes</Text>
        },
    },
    Favorites: {
        screen: FavoriteNavigationConfig,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <MaterialIcons name='favorite' size={25} color={tabInfo.tintColor}/>
            },
            tabBarLabel: <Text style={{fontFamily: 'open-sans-bold'}}>Favorites</Text>
        }
    }
}

const AppBottomTabsNavigator = Platform.OS === 'android' ?
    createMaterialBottomTabNavigator(tabConfig, {
        activeColor: 'white',
        shifting: true,
        barStyle: {
            backgroundColor: Colors.primary
        }
    })
    : createBottomTabNavigator(tabConfig, {
        tabBarOptions: {
            activeTintColor: Colors.primary,
            labelStyle: {
                fontFamily: 'open-sans-bold',
            },
        }
    })

const MainNavigator = createDrawerNavigator({
    AppNavigator: {
        screen: AppBottomTabsNavigator,
        navigationOptions: {
            drawerLabel: 'Recipes'
        }
    },
    Filters: FilterNavigationConfig
}, {
    contentOptions: {
        activeTintColor: Colors.primary,
        labelStyle: {
            fontFamily: 'open-sans-bold',
        }
    }
})

export default createAppContainer(MainNavigator);
