import {RECIPES} from "../../data/dummy-data";
import {TOGGLE_FAVORITE, SET_FILTERS} from "../actions/recipes";

const initialState = {
    recipes: RECIPES,
    filteredRecipes: RECIPES,
    favoriteRecipes: []
};

const recipeReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_FAVORITE:
            const indexOfFavorite = state.favoriteRecipes.findIndex(recipe => recipe.id === action.recipeID);
            if (indexOfFavorite >= 0) {
                const updatedFavoriteRecipes = [...state.favoriteRecipes];
                updatedFavoriteRecipes.splice(indexOfFavorite, 1);

                return {
                    ...state,
                    favoriteRecipes: updatedFavoriteRecipes
                }
            } else {
                const recipe = state.recipes.find(recipe => recipe.id === action.recipeID);
                return {
                    ...state,
                    favoriteRecipes: [...state.favoriteRecipes, recipe]
                }
            }
        case SET_FILTERS:
            const filteredRecipes = state.recipes.filter(recipe => {
                if (action.filters.glutenFree && !recipe.isGlutenFree) {
                    return false;
                }

                if (action.filters.lactoseFree && !recipe.isLactoseFree) {
                    return false;
                }

                return true;
            })
            return {
                ...state,
                filteredRecipes
            }
        default:
            return state;
    }
}

export default recipeReducer
