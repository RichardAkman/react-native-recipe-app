import React from 'react';
import {FlatList} from 'react-native';
import {CATEGORIES} from '../data/dummy-data'
import CategoryItem from "../components/CategoryItem";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

const CategoriesScreen = props => {

    const renderItem = (itemData) => {
        return <CategoryItem
            title={itemData.item.title}
            color={itemData.item.color}
            onSelect={() => {
                props.navigation.navigate({
                    routeName: 'CategoryMeals',
                    params: {
                        categoryId: itemData.item.id,
                    }
                })
            }}/>
    }

    return (
        <FlatList numColumns={2} data={CATEGORIES} renderItem={renderItem}/>
    )
};

CategoriesScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Recipe categories',
        headerLeft: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
                title='no'
                iconName='ios-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer()
                }}
            />
        </HeaderButtons>
    }
}

export default CategoriesScreen;
