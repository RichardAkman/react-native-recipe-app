import React from 'react';
import {CATEGORIES} from '../data/dummy-data'
import RecipeList from "../components/RecipeList";
import {useSelector} from "react-redux";
import {View, StyleSheet} from "react-native";
import DefaultText from "../components/DefaultText";

const CategoryMealsScreen = props => {
    const categoryId = props.navigation.getParam('categoryId');

    const allRecipes = useSelector(state => {
        return state.recipes.filteredRecipes
    })

    const recipes = allRecipes.filter(recipe => {
        return recipe.categoryIds.indexOf(categoryId) >= 0
    })

    if (recipes.length === 0) {
        return <View style={styles.noData}>
            <DefaultText>No recipes to show</DefaultText>
        </View>
    }

    return (
        <RecipeList recipes={recipes} navigation={props.navigation}/>
    )
};

const styles = StyleSheet.create({
    noData: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
CategoryMealsScreen.navigationOptions = (navigationData) => {
    const categoryId = navigationData.navigation.getParam('categoryId');

    const categoryData = CATEGORIES.find(category => {
        return category.id === categoryId
    })

    return {
        headerTitle: categoryData.title,
    }
}


export default CategoryMealsScreen;
