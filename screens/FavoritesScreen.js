import React from 'react';
import {StyleSheet, View} from 'react-native';
import RecipeList from "../components/RecipeList";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import {useSelector} from "react-redux";
import DefaultText from "../components/DefaultText";

const FavoritesScreen = props => {
    const favoriteRecipes = useSelector(state => {
        return state.recipes.favoriteRecipes
    })

    if (favoriteRecipes.length === 0) {
        return <View style={styles.noFavorites}>
            <DefaultText>No favorites</DefaultText>
        </View>
    }

    return (
        <RecipeList recipes={favoriteRecipes} navigation={props.navigation}/>
    )
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    noFavorites: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

FavoritesScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Favorite recipes',
        headerLeft: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
                title='no'
                iconName='ios-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer()
                }}
            />
        </HeaderButtons>
    }
}


export default FavoritesScreen;
