import React, {useState, useEffect, useCallback} from 'react';
import {StyleSheet, View, Switch} from 'react-native';
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import DefaultText from "../components/DefaultText";
import Colors from "../constants/colors";
import {useDispatch} from "react-redux";
import {setFilters} from "../redux/actions/recipes";

const FilterSwitch = props => {
    return (
        <View style={styles.filterContainer}>
            <DefaultText>
                {props.filter}
            </DefaultText>
            <Switch
                value={props.value}
                onValueChange={newVal => props.valueUpdater(newVal)}
                trackColor={{
                    true: Colors.primary
                }}
                thumbColor={Colors.primary}
            />
        </View>
    )
}

const FiltersScreen = props => {
    const {navigation} = props;
    const [isGlutenFree, setIsGlutenFree] = useState(false);
    const [isLactoseFree, setIsLactoseFree] = useState(false);

    const dispatch = useDispatch();

    const saveFilterState = useCallback(() => {
        const appliedFilters = {
            glutenFree: isGlutenFree,
            lactoseFree: isLactoseFree
        }

        dispatch(setFilters(appliedFilters))
    }, [isGlutenFree, isLactoseFree, dispatch]);

    useEffect(() => {
        navigation.setParams({
            save: saveFilterState
        });
    }, [saveFilterState])

    return (
        <View style={styles.screen}>
            <DefaultText style={styles.title}>Filters</DefaultText>
            <FilterSwitch filter='gluten-free' value={isGlutenFree} valueUpdater={setIsGlutenFree}/>
            <FilterSwitch filter='lactose-free' value={isLactoseFree} valueUpdater={setIsLactoseFree}/>
        </View>
    )
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 22,
        textAlign: 'center',
        margin: 5
    },
    filterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '80%',
        marginVertical: 10
    }
});

FiltersScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Filters',
        headerLeft: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
                title='no'
                iconName='ios-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer()
                }}
            />
        </HeaderButtons>,
        headerRight: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
                title='no'
                iconName='ios-save'
                onPress={navData.navigation.getParam('save')}
            />
        </HeaderButtons>,
    }
}

export default FiltersScreen;
