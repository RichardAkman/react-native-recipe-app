import React, {useEffect, useCallback} from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    Image
} from 'react-native';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from "../components/HeaderButton";
import DefaultText from "../components/DefaultText";
import {useSelector, useDispatch} from "react-redux";
import {toggleFavorite} from '../redux/actions/recipes'

const ListItem = props => {
    return (
        <View style={styles.listItem}>
            <DefaultText>{props.children}</DefaultText>
        </View>
    )
}

const CategoryMealsScreen = props => {
    const recipeID = props.navigation.getParam('recipeID');

    const allRecipes = useSelector(state => {
        return state.recipes.recipes
    })

    const isFavoriteRecipe = useSelector(state => {
        return state.recipes.favoriteRecipes.some(recipe => recipe.id === recipeID)
    })

    const recipe = allRecipes.find(recipe => {
        return recipe.id === recipeID
    })

    const dispatch = useDispatch();

    const toggleFavoriteHandler = useCallback(() => {
        dispatch(toggleFavorite(recipeID))
    }, [dispatch, recipeID])

    useEffect(() => {
        props.navigation.setParams({toggleFavorite: toggleFavoriteHandler})
    }, [toggleFavoriteHandler]);

    useEffect(() => {
        props.navigation.setParams({isFavorite: isFavoriteRecipe})
    }, [isFavoriteRecipe])

    return (
        <ScrollView>
            <Image
                source={{uri: recipe.imageUrl}}
                style={styles.image}
            />
            <View style={styles.details}>
                <DefaultText>{recipe.duration}</DefaultText>
                <DefaultText>{recipe.complexity}</DefaultText>
                <DefaultText>{recipe.affordability}</DefaultText>
            </View>
            <View>
                <DefaultText style={styles.title}>
                    Ingredients
                </DefaultText>
                {recipe.ingredients.map(ingredient => {
                    return <ListItem key={ingredient}>{ingredient}</ListItem>
                })}
            </View>
            <View>
                <DefaultText style={styles.title}>
                    Steps
                </DefaultText>
                {recipe.steps.map(step => {
                    return <ListItem key={step}>{step}</ListItem>
                })}
            </View>
        </ScrollView>
    )
};

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    details: {
        flexDirection: 'row',
        padding: 15,
        justifyContent: 'space-around',
    },
    title: {
        fontFamily: 'open-sans-bold',
        textAlign: 'center'
    },
    listItem: {
        marginVertical: 10,
        marginHorizontal: 15,
        borderColor: '#ccc',
        borderWidth: 3,
        padding: 5
    }
});


CategoryMealsScreen.navigationOptions = (navigationData) => {
    const recipeTitle = navigationData.navigation.getParam('recipeTitle');
    const toggleFavorite = navigationData.navigation.getParam('toggleFavorite');
    const isFavorite = navigationData.navigation.getParam('isFavorite');

    return {
        headerTitle: recipeTitle,
        headerRight: () => <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item
                title='no'
                iconName={isFavorite ? 'ios-star' : 'ios-star-outline'}
                onPress={toggleFavorite}
            />
        </HeaderButtons>
    }
}


export default CategoryMealsScreen;
